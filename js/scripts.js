function scrollTop() {
  const windowH = document.body.clientHeight
  const btnUp = $('.js-btnScrollTop')
  btnUp.on('click', () => window.scrollTo({top: 0, behavior: "smooth"}))
  $(window).on('scroll', (e) => {
    if (window.pageYOffset > windowH) {
      btnUp.addClass('on')
    } else {
      btnUp.removeClass('on')
    }

  })
}

$(document).ready(function() {
  var body = document.body,
    html = document.documentElement;

  var documentHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);

  // Evaluating parent document height
  var menuBody = $('.analyzes__nav');
  var menuHeight = menuBody.offset().top + menuBody.outerHeight(true);
  $(".analyzes .drawer-toggle").sticky({topSpacing: 0})
  $(".analyzes__nav-inner").sticky({
    topSpacing: 50,
    bottomSpacing: documentHeight - menuHeight
  });

  $('.js-slider').bxSlider();

  $('.js-slider-galley').bxSlider({minSlides: 1, maxSlides: 12, slideWidth: 353, pager: false, slideMargin: 14});

  $('a[data-modal-img]').click(function(event) {
    const src = $(this).attr('href')
    const newHTML = `<div><img src=${src} /></div>`
    $(newHTML).appendTo('body').modal({modalClass: 'modal modal_img'});

    return false;
  });

  $('#js-form-feedback form').on('submit', () => {
    $('#js-form-feedback-success').modal();
  })

  scrollTop()
});
